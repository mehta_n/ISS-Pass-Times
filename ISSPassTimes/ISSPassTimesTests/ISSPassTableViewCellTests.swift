import XCTest
@testable import ISSPassTimes

class ISSPassTableViewCellTests: XCTestCase {
    let passTableViewCell = ISSPassTableViewCell()

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAwakeFromNib(){
        passTableViewCell.awakeFromNib()
    }
    
    func testSetSelected(){
        passTableViewCell.setSelected(true, animated: true)
    }
}
