import XCTest
import CoreLocation
@testable import ISSPassTimes

class ISSPassStationsVCTests: XCTestCase {
    var controller : ISSPassStationsVC!

    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        controller = storyboard.instantiateViewController(withIdentifier: "ISSPassStationsVC") as! ISSPassStationsVC
        let _ = controller.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testShowLocationPermissionAlert(){
        controller.showLocationPermissionAlert()
    }
    
    func testViewDidAppear(){
        controller.viewDidAppear(true)
    }
    
    func testGetCurrentLocation(){
        controller.startUpdateLocation()
    }
    
    func testTrackingFailError(){
        let maybeError = NSError(domain: "", code: 0, userInfo: ["message" : "Object does not exist"])
        controller.trackLocationFailure(maybeError)
    }
    
    func testTracingLocation(){
        let currentLocation: CLLocation? = nil
        controller.trackUpdatedLocation(currentLocation)
    }
    
    func testNumberOfRow(){
        _ = controller.tableView(UITableView(), numberOfRowsInSection: 0)
    }
    
    func testOnReceiveResponse(){
        controller.onReceiveResponse(response: ISSResponseModel())
    }
    
    func testGetPassService(){
        ISSWebServices.getPassService(77.580643, long: 12.972442, onSuccess: { (response, serviceIdentifier) in
        }, onError: { (error, serviceIdentifier) in
        })
    }
    
    func testModelView(){
        let kMessageStatus = "success"
        
        let testBundle = Bundle(for: type(of: self))
        let path = testBundle.path(forResource: "ISSResponseModelTest", ofType: "json")
        let responseData = ISSPassStationsVCTests.jsonForFileAtPath(path!)
        if let rsponse = try? JSONDecoder().decode(ISSResponseModel.self, from: responseData!) {
            XCTAssertEqual(rsponse.message, kMessageStatus)
            controller.responseDataArray = rsponse.response

            let indexPath = NSIndexPath(row: 0, section: 0) as IndexPath
            XCTAssertNotNil(controller?.tableView(controller.passTableView, cellForRowAt: indexPath))
        }
    }
    
    open class func jsonForFileAtPath(_ filePath:String) -> Data?{
        do {
            let text = try NSString(contentsOfFile: filePath,encoding: String.Encoding.ascii.rawValue)
            return text.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)
        } catch {
            // handle error
            print("Catch block executed")
        }
        return nil
    }
}

