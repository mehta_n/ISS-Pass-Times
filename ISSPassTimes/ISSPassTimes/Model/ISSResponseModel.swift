import UIKit

class ISSResponseModel: Codable {
    //MARK: Properties
    var message: String?
    var response: [ISSResponseDataModel] = [ISSResponseDataModel]()
}

class ISSResponseDataModel:Codable {
    //MARK: Properties
    var duration: UInt64?
    var risetime: UInt64?
}
