import UIKit
import Foundation

/// App Name
let AppName = "ISS Pass Times"

/// Date Formatter 
let dateFormatConstant = "MMM dd YYYY hh:mm a"

/// Utility Class for Common Functions.
class ISSUtility : NSObject{
    
    /// Timestamp to Convert in DateTime
    ///
    /// - Parameter time: Timestamp - Int
    /// - Returns: DateTime - String
    class func timeStringFromUnixTime(time:UInt64) -> String{
        let date = NSDate(timeIntervalSince1970: TimeInterval(time))
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = dateFormatConstant
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
    
    /// Open iPhone Settings for Location Permission Change
    class func openSettings(){
        if #available(iOS 10.0, *){
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
        }else{
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }
    }
}

// MARK: - String Extension
extension String {
    /// Localization String
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}

