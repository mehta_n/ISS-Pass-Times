import Foundation
import CoreLocation

/// Protocol for Handle Update Location
protocol ISSLocationManagerDelegate {
    func trackUpdatedLocation(_ currentLocation: CLLocation?)
    func trackLocationFailure(_ error: NSError)
}

/// Location Manager Class
class ISSLocationManager: NSObject, CLLocationManagerDelegate {
    static let sharedInstance: ISSLocationManager = {
        let instance = ISSLocationManager()
        return instance
    }()

    var locationManager: CLLocationManager?
    var currentLocation: CLLocation?
    var delegate: ISSLocationManagerDelegate?

    // MARK: - Initiation
    /// Implemented by subclasses to initialize a new object (the receiver) immediately after memory for it has been allocated.
    override init() {
        super.init()

        self.locationManager = CLLocationManager()
        guard let locationManager = self.locationManager else {
            return
        }
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            // you have 2 choice 
            // 1. requestAlwaysAuthorization
            // 2. requestWhenInUseAuthorization
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.desiredAccuracy = kCLLocationAccuracyBest // The accuracy of the location data
        locationManager.delegate = self
    }
    
    // MARK: - Start Update Location
    func startUpdatingLocation() {
        self.locationManager?.startUpdatingLocation()
    }
    
    // MARK: - Stop Update Location
    func stopUpdatingLocation() {
        self.locationManager?.stopUpdatingLocation()
    }
    
    // CLLocationManagerDelegate
    /// the delegate that new location data is available. Implementation of this method is optional but recommended.
    ///
    /// - Parameters:
    ///   - manager: The location manager object that generated the update event.
    ///   - locations: An array of CLLocation objects containing the location data.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        if currentLocation == nil {
            // singleton for get last(current) location
            currentLocation = location
            // use for real time update location
            updateLocation(currentLocation)
        }
        currentLocation = nil
    }
    
    /// Tells the delegate that the location manager was unable to retrieve a location value.
    ///
    /// - Parameters:
    ///   - manager: The location manager object that was unable to retrieve the location.
    ///   - error: The error object containing the reason the location or heading could not be retrieved.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // do on error
        updateLocationFail(error as NSError)
    }
    
    // MARK: -  Private function
    /// Get Current Updated Location and Pass to VC.
    ///
    /// - Parameter currentLocation: The latitude, longitude, and course information reported by the system.
    fileprivate func updateLocation(_ currentLocation: CLLocation?){
        if let delegate = self.delegate{
            delegate.trackUpdatedLocation(currentLocation)
        }
    }
    
    /// Get Current Updated Location and Pass to VC.
    ///
    /// - Parameter error: Information about an error condition including a domain, a domain-specific error code, and application-specific information.
    fileprivate func updateLocationFail(_ error: NSError) {
        if let delegate = self.delegate {
            delegate.trackLocationFailure(error)
        }
    }
}
