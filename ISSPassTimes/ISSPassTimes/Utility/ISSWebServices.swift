import UIKit

struct ServiceIdentifiers {
    /// Base URL
    static let baseServiceURL = "http://api.open-notify.org/"
    
    /// Get Station Pass
    static let getStationsService = "iss-pass.json"
}

/// Common Web Service Class
class ISSWebServices: NSObject {
    
    /// Get Station Pass From Server API
    ///
    /// - Parameters:
    ///   - lat: Current Latitude
    ///   - long: Current Longitude
    ///   - onSuccess: Success block
    ///   - onError: Failure block
    class func getPassService( _ lat : Double, long : Double,
                         onSuccess: @escaping (_ response:ISSResponseModel,_ serviceIdentifier:String)->(),
                         onError: @escaping (_ error:Error?,_ serviceIdentifier:String)->()){
        
        let sessionUrl = ServiceIdentifiers.getStationsService + "?lat=\(lat)&lon=\(long)"
        let service = ISSBaseService(identifier: ServiceIdentifiers.getStationsService)
        service.makeServiceRequestWithUrl(url: sessionUrl, requestType: "GET", body: [:], success: { (response) in
            if let responseData = try? JSONDecoder().decode(ISSResponseModel.self, from: response) {
                onSuccess(responseData, service.serviceIdentifier)
            }else{
                onError(nil,service.serviceIdentifier)
            }
        }) { (errorResponse) in
            onError(errorResponse,service.serviceIdentifier)
        }
    }
    
}

