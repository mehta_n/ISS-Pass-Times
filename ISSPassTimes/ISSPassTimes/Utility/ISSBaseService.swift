import UIKit

class ISSBaseService: NSObject, URLSessionDelegate {
    
    var serviceIdentifier: String!
    
    /**
     Convenience Init Method for initialization of variable identifier
     */
    convenience init(identifier: String) {
        self.init()
            
        // Save Service Identifier
        serviceIdentifier = identifier
    }
    
    /**
     This method makes the service request
     
     - Parameters:
     - url: URL Base String
     - requestType: request Type
     - body: Body in the form of an array
     
     - returns:
     - success: Success block
     - failure: Failure block
     */
    func makeServiceRequestWithUrl(url: String,
                                   requestType: String?,
                                   body: [String: Any],
                                   success: @escaping (_ response: Data) -> Void,
                                   failure: @escaping (_ error: Error?) -> Void) -> Void {
        let request : NSMutableURLRequest = createRequest(url, body: body as [String : AnyObject]?,requestType:requestType!)
        
        let configuration = URLSessionConfiguration.default
        
        configuration.timeoutIntervalForRequest = 30     // Timeout for Service
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        configuration.urlCache = nil
        
        let session:Foundation.URLSession
        session = Foundation.URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        let currentTask:URLSessionDataTask = session.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error -> Void in
            if error != nil || data == nil {
                failure(error)
            }else {
                success(data!)
                return
            }
        })
        currentTask.resume()
    }
    
    /**
     This method makes the service request
     
     - Parameters:
     - url: Request url
     - body: Body in the form of an array
     - requestType: Type of request POST/GET
     
     - returns:
     - NSMutableURLRequest parameter
     */
    func createRequest(_ url: String,
                       body: [String: AnyObject]?,
                       requestType:String) -> NSMutableURLRequest {
        
        let urlString: String = ServiceIdentifiers.baseServiceURL + url
        let request: NSMutableURLRequest = NSMutableURLRequest()
        // Add final request url
        request.url = URL(string: urlString)
        request.httpMethod = requestType
        
        // Check for Request Type
        if requestType == "GET" {
            // Request should be having empty body for get
            request.httpBody = Data()
        }
        else {
            // Add HTTPBody form Dictionary to JSON
            let httpBody = try! JSONSerialization.data(withJSONObject: body!, options: .prettyPrinted)
            let JSONString: NSString = NSString(data: httpBody, encoding: String.Encoding.utf8.rawValue)!
            request.httpBody = JSONString.data(using: String.Encoding.utf8.rawValue)
        }
        return request
    }
}

