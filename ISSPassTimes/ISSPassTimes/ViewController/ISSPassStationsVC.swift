import UIKit
import CoreLocation

class ISSPassStationsVC: UIViewController, ISSLocationManagerDelegate,UITableViewDelegate, UITableViewDataSource {
    //MARK: Properties
    @IBOutlet weak var passTableView: UITableView!
    var responseDataArray:[ISSResponseDataModel] = [ISSResponseDataModel]()
    
    // MARK: - View Controller Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: String(describing: ISSPassTableViewCell.self), bundle: nil)
        passTableView.register(nib, forCellReuseIdentifier: String(describing: ISSPassTableViewCell.self))
        
        //Set Delegate For Location Manager Class
        ISSLocationManager.sharedInstance.delegate = self
        ISSLocationManager.sharedInstance.startUpdatingLocation()
        
        if CLLocationManager.authorizationStatus() == .denied {
            showLocationPermissionAlert()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(startUpdateLocation), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    // MARK: - Denied Permission Alert
    /// - If user Denied Location Authorization and again open app then Ask Settings and Cancel Popup.
    /// - Tap on Setting - User Natigate App Setting to change permission.
    func showLocationPermissionAlert(){
        let alert = UIAlertController(title: AppName, message: "ISS_Location".localized, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancel".localized, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings".localized, style: .default, handler: { (action) in
            ISSUtility.openSettings()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Location Methods
    /// Start Update Location
    @objc func startUpdateLocation(){
        ISSLocationManager.sharedInstance.delegate = self
        ISSLocationManager.sharedInstance.startUpdatingLocation()
    }
    
    /// Success Update Location Delegate
    ///
    /// - Parameter currentLocation: The latitude, longitude, and course information reported by the system.
    func trackUpdatedLocation(_ currentLocation: CLLocation?) {
        if let currentLocation = currentLocation{
            //Finally stop updating location otherwise it will come again and again in this delegate
            ISSLocationManager.sharedInstance.stopUpdatingLocation()
            ISSWebServices.getPassService(currentLocation.coordinate.latitude, long: currentLocation.coordinate.longitude, onSuccess: { (response, serviceIdentifier) in
                self.onReceiveResponse(response: response)
            }, onError: { (error, serviceIdentifier) in
                // error alert
                let alert = UIAlertController(title: AppName, message: "ServiceError".localized, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            })
            
        }
    }
    
    /// Failure Update Location Delegate
    ///
    /// - Parameter error: Information about an error condition including a domain, a domain-specific error code, and application-specific information.
    func trackLocationFailure(_ error: NSError) {
        print("tracing Location Error : \(error.description)")
    }
    
    /// Success Handler Response
    ///
    /// - Parameter response: Model Class For Display Data
    func onReceiveResponse(response: ISSResponseModel){
        responseDataArray = response.response
        passTableView.reloadData()
    }
    
    //MARK: - Table View Methods
    /// Return the number of rows in a given section of a table view.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseDataArray.count
    }
    
    /// The data source for a cell to insert in a particular location of the table view.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:ISSPassTableViewCell = passTableView.dequeueReusableCell(withIdentifier: String(describing: ISSPassTableViewCell.self)) as! ISSPassTableViewCell!
        
        // Fetches the appropriate data for the data source layout.
        let responseObject = responseDataArray[indexPath.row]
        
        cell.durationLabel!.text = "\(responseObject.duration ?? 0)" + "Seconds".localized
        cell.timeLabel!.text = ISSUtility.timeStringFromUnixTime(time: responseObject.risetime ?? 0)
        
        return cell
    }
}
