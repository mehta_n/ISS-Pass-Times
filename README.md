Project : International Space Station Passes (Code Challenges)

App Name : ISS Pass Times

Technology/Tool : Xcode 9.2, Support Swift 4.0

Support : All iPhone Device

Orientation : Portrait

Project Structure :
1) View Controller Class
2) Storyboard
3) Location Manager Class
4) Web-service Manager Class
5) Localizable String
6) Utility Class
7) Model Class
8) Unit Test Cases Class
9) Documentation Files (ReadMe, Screenshot)

Project :
-> The project developed using native iOS Framework. There is no any 3rd Party Framework/Dependency.
-> Added prefix "ISS" (International Space Station) for all classes.

Note :
If you are checking in Simulator then please select location.
